#include<stdio.h> /* for input/output */
#include <stdlib.h>
#include <time.h>
#include<mpi.h> /* for mpi routines */
#include<openssl/md5.h>
#include <string.h>
#define BUFSIZE 64 /* The size of the messege being passed */

unsigned char *MD5(const unsigned char *d, unsigned long n,
                  unsigned char *md);

int MD5_Init(MD5_CTX *c);
int MD5_Update(MD5_CTX *c, const void *data,
                  unsigned long len);
int MD5_Final(unsigned char *md, MD5_CTX *c);

/**
 * Used to calculate md5 hash for an input string
 * 
 * */
char* str2md5(const char* str, int length) {
    int n;
    MD5_CTX c;
    unsigned char digest[16];
    char* out = (char*)malloc(33);

    MD5_Init(&c);

    while (length > 0) {
        if (length > 512) {
            MD5_Update(&c, str, 512);
        } else {
            MD5_Update(&c, str, length);
        }
        length -= 512;
        str += 512;
    }

    MD5_Final(digest, &c);

    for (n = 0; n < 16; ++n) {
        snprintf(&(out[n*2]), 16*2, "%02x", (unsigned int)digest[n]);
    }

    return out;
}

char getRandomASCIIChar(char* dict) {
   int r = rand() % 36;
   return dict[r];
}

char *mkrndstr(size_t length) { // const size_t length, supra

static char charset[] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789,.-#'?!"; // could be const
char *randomString;

if (length) {
    randomString = malloc(length +1); // sizeof(char) == 1, cf. C99

    if (randomString) {
        int l = (int) (sizeof(charset) -1); // (static/global, could be const or #define SZ, would be even better)
        int key;  // one-time instantiation (static/global would be even better)
        for (int n = 0;n < length;n++) {        
            key = rand() % l;   // no instantiation, just assignment, no overhead from sizeof
            randomString[n] = charset[key];
        }

        randomString[length] = '\0';
    }
}

return randomString;
}

int main(int argc, char** argv) {
    MPI_Request isreq, irreq;
    double start,finish;
    time_t t;
	int n_processes; /* the total number of processes */
    int doWork;
    int finishedWork = 0;
	int tag=0; /* not important here */
    MPI_Status status; /* not important here */
    /* Intializes random number generator */
    srand((unsigned) time(&t));
    int my_rank; /* the rank of this process */
    int targetLength = 2;
    char alphabet[37] = "qwertyuiopasdfghjklzxcvbnm1234567890";
    char* targetHash = str2md5("pa", 2);;
    MPI_Init(&argc, &argv); /* Initializing mpi */
    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank); /* Getting my rank */
	MPI_Comm_size(MPI_COMM_WORLD, &n_processes); /* Getting # of processes */
    start=MPI_Wtime();
    //printf(targetHash);
    //char* randstr = mkrndstr(4);
    // printf(randstr);
    // return 0;

    if (my_rank == 0) { // master process
        doWork = 1;
        while(1 == 1) {
            MPI_Bcast(&doWork, 1, MPI_INT, 0, MPI_COMM_WORLD);

            for (int i = 1; i < n_processes; i++) {
                MPI_Recv(&finishedWork, 1, MPI_INT, i, tag, MPI_COMM_WORLD, &status);
                
                if (finishedWork == 1) {
                    printf("received results %i: \n", finishedWork);
                    doWork = 0;
                    MPI_Bcast(&doWork, 1, MPI_INT, 0, MPI_COMM_WORLD);
                    break;
                }
            }

            if (finishedWork == 1) {
                break;
            }
			
        }
    } else { // worker
        srand((unsigned) time(&t) + my_rank);
        int z = 0;
        while (1 == 1) {
            MPI_Bcast(&doWork, 1, MPI_INT, 0, MPI_COMM_WORLD);
            if (doWork == 0) {
                break;
            }

         //   printf("doing work %i: \n", my_rank);
/*
            char randChars[targetLength + 1];
            for (int i = 0; i < targetLength; i++) {
                char c = getRandomASCIIChar(alphabet);
                randChars[i] = c;
            }
            randChars[targetLength + 1] = '\0';
            */
            char* randstr = mkrndstr(targetLength);

          //  printf(randstr);
        //    printf("\n");

            char* hash = str2md5(randstr, targetLength);
            if (strcmp(hash, targetHash) == 0) {
                printf("Found target string: \n");
                printf(randstr);
                finishedWork = 1;
                free(hash);
                free(randstr);
                MPI_Send(&finishedWork, 1, MPI_INT, 0, 0, MPI_COMM_WORLD);
                break;
            }
           // printf("sending response: \n");
            MPI_Send(&finishedWork, 1, MPI_INT, 0, 0, MPI_COMM_WORLD);
            
            free(randstr);
            free(hash);
        }
    }

    MPI_Finalize();
    return 0;

}