/*
* The root node sends out a message to the next node in the ring and
* each node then passes the message along to the next node. The root
* node times how long it takes for the message to get back to it.
*/

#include<stdio.h> /* for input/output */
#include<mpi.h> /* for mpi routines */
#define BUFSIZE 64 /* The size of the messege being passed */

int main( int argc, char** argv)
{
	double start,finish;
	int my_rank; /* the rank of this process */
	int n_processes; /* the total number of processes */
	char buf[BUFSIZE]; /* a buffer for the messege */
	int tag=0; /* not important here */
	MPI_Status status; /* not important here */
	MPI_Init(&argc, &argv); /* Initializing mpi */
	MPI_Comm_size(MPI_COMM_WORLD, &n_processes); /* Getting # of processes */
	MPI_Comm_rank(MPI_COMM_WORLD, &my_rank); /* Getting my rank */
    /*
    * If this process is the root process send a messege to the next node
    * and wait to recieve one from the last node. Time how long it takes
    * for the messege to get around the ring. If this process is not the
    * root node, wait to recieve a messege from the previous node and
    * then send it to the next node.
    */
	start=MPI_Wtime();
	printf("Hello world! I am %d of %d\n", my_rank, n_processes);
	if( my_rank == 0 )
	{
		/* send to the next node */
		MPI_Send(buf, BUFSIZE, MPI_CHAR, my_rank+1, tag, MPI_COMM_WORLD);
		/* receive from the last node */
		MPI_Recv(buf, BUFSIZE, MPI_CHAR, n_processes-1, tag,
		MPI_COMM_WORLD, &status);
	}
    if( my_rank != 0)
	{
		/* receive from the previous node */
		MPI_Recv(buf, BUFSIZE, MPI_CHAR, my_rank-1, tag,
		MPI_COMM_WORLD, &status);
		/* send to the next node */
		MPI_Send(buf, BUFSIZE, MPI_CHAR, (my_rank+1)%n_processes, tag,
		MPI_COMM_WORLD);
	}
	finish=MPI_Wtime();
	MPI_Finalize(); /* I’m done with mpi stuff */
	/* Print out the results. */
	if (my_rank == 0)
	{
		printf("Total time used was %f seconds\n", finish-start);
	}
	return 0;
}
